import moment from "moment";
import {getUserNameByID, postThread} from "./helper";
import {
  insertNewCurrentOrder, 
  getLatestCurrentOrder, 
  getCurrentOrders, 
  updateCurrentOrderSlackMsgId, 
  deleteCurrentOrderByMsgId, 
  deleteCurrentOrderById, 
  insertUserOrder, 
  insertGroupOrder,
  getOrdersPriceSum,
  getOrderMenus,
  getNotPaidUserOrder,
  markAllUserOrdersWithPaid,
  markUserOrderWithPaid,
  getNotPaidUserOrderAsPurchaser,
  markAllUserOrdersWithPaidAsPurchaser,
  markUserOrderWithPaidAsPurchaser,
  getPaidUserOrder,
  markAllUserOrdersWithNotPaid,
  markUserOrderWithNotPaid,
  getPaidUserOrderAsPurchaser,
  markAllUserOrdersWithNotPaidAsPurchaser,
  markUserOrderWithNotPaidAsPurchaser
} from "./db";
import axios from "axios";
import TOKEN from "./tokens";
import logger from "./logger.mjs";
import hitomi from "./restaurant.mjs";

export async function handleMessageOrder(message, userID, bot) {
  const messageWords = message.split(/\s+/);
  const restaurant = messageWords[messageWords.indexOf("-o") + 1].toLowerCase();
  const index = messageWords.indexOf("-t");
  let deadline = null;
  let dbRestaurant = await getCurrentOrders();
  dbRestaurant = dbRestaurant.map((item) => item.restaurant).find(item => item === restaurant);

  if(dbRestaurant === undefined){
    if (index !== -1) {
      if (messageWords[index + 1]) {
        deadline = moment(messageWords[index + 1], "HH:mm");
      } else {
        throw new Error("Missing deadline argument");
      }
    } else {
      throw new Error("Order has not deadline command");
    }
    const now = moment();
    if (deadline.isAfter(now)) {
      insertNewCurrentOrder(deadline, userID, restaurant);
      deadline = moment(deadline).format("HH:mm");
      const outputMessage = `<!channel> Bestellung bei "${restaurant}", bitte bis ${deadline} abgeben!`;
      bot.postMessageToChannel("food-supply", outputMessage, null);
    } else {
      throw new Error(
        "The set time is samller than the current time, please make a new order."
      );
    }
  } else {
    throw new Error("there is currently an order for this restaurant");
  }
}

export async function handleOrder(messageID, message, channel, bot) {
  const regex = /Bestellung bei "(.*)"/;
  const result = regex.exec(message);
  const restaurant = result[1];
  logger.info("Handling order for restaurant '" + restaurant + "'");
  await updateCurrentOrderSlackMsgId(messageID, restaurant);
  const newestOrder = await getLatestCurrentOrder(restaurant);
  logger.info("Found order", newestOrder);
  const timeDiff = moment(newestOrder.deadline).diff(moment());
  logger.info("Sending order in " + timeDiff / 1000 + " seconds");
  setTimeout(
    () => handleOrderMessage(messageID, newestOrder.purchaser, channel, restaurant, bot),
    timeDiff
  );
}

export async function handleCurrentOrder(localPartMailText, bot) {
  let outputMessage;
  const currentOrder = await getCurrentOrders();
  let deadline;
  if (currentOrder.length >= 1) {
    currentOrder.map((order, idx) => {
      if (idx === 0){
        deadline = moment(order.deadline).format("HH:mm");
        outputMessage = `OrderID: ${order.id}, Restaurant: ${order.restaurant}, Deadline: ${deadline}\n`;
      } else {
        deadline = moment(order.deadline).format("HH:mm");
        outputMessage = outputMessage + `OrderID: ${order.id}, Restaurant: ${order.restaurant}, Deadline: ${order.deadline}\n`;
      }
    });
    bot.postMessageToUser(localPartMailText, outputMessage, null);
  } else {
    outputMessage = "Es gibt derzeit keine laufenden Bestellungen";
    bot.postMessageToUser(localPartMailText, outputMessage, null);
    throw new Error("There are no current orders!");
  }
}

export async function cancelCurrentOrder(message, localPartMailText, userID, bot) {
  let outputMessage;
  const currentOrder = await getCurrentOrders();
  const user = currentOrder.find(item => item.purchaser === userID);
  if(user !== undefined){
    if (currentOrder.length >= 1) { 
      const messageWords = message.split(/\s+/);
      let cancelArg = messageWords[messageWords.indexOf("-cancel") + 1];
      cancelArg = parseInt(cancelArg);
      const deletedOrder = await deleteCurrentOrderById(cancelArg);
      if (deletedOrder.changes === 0){
        outputMessage = `Order mit der ID: ${cancelArg} wurde nicht abgebrochen!`;
        bot.postMessageToUser(localPartMailText, outputMessage, null);
        throw new Error(`Order with ID: ${cancelArg} couldn't be canceled!`);
      } else {
        outputMessage = `Order mit der ID: ${cancelArg} wurde abgebrochen!`;
        bot.postMessageToUser(localPartMailText, outputMessage, null);
      }
    } else {
      console.log(
        "No running orders or wrong sytnax, there is nothing to cancel."
      );
    }
  } else {
    outputMessage = "Sie können die Bestellung nicht stornieren weil sie nicht die Bestellung aufgegeben haben.";
    bot.postMessageToUser(localPartMailText, outputMessage, null);
  }
}

export async function showInformation(message, localPartMailText, userID, bot) {
  let outputMessage;
  const messageWords = message.split(/\s+/);
  const showArg = messageWords[messageWords.indexOf("-s") + 1].toLowerCase();
  let days = messageWords[messageWords.indexOf("-s") + 2];
  days = parseInt(days);
  const user = getUserNameByID(bot, userID);

  if (showArg === "price" && isNaN(days) === false) {
    let priceSum = await getOrdersPriceSum(user, days);
    priceSum = priceSum.sum_price;
    outputMessage = `Die Summe von Menüs der letzte/n ${days} Tage beträgt ${priceSum}€`;
    bot.postMessageToUser(localPartMailText, outputMessage, null);

  } else if (showArg === "menu" && isNaN(days) === false) {
    // TODO: summ up menus like with sms-menus
    const menus = await getOrderMenus(user, days);
    menus.map((item, idx) => {
      if (idx === 0){
        outputMessage = `${item.res}, ${item.menu}, Price: ${item.price}€\n`;
      } else {
        outputMessage = outputMessage + `${item.res}, ${item.menu}, Price: ${item.price}€\n`;
      }
    });
    bot.postMessageToUser(localPartMailText, outputMessage, null);

  } else {
    const outputMessage = "Falsche Argumente '-s' mitgegeben";
    bot.postMessageToUser(localPartMailText, outputMessage, null);
    throw new Error("Wrong Argument parsed for showInformation");
  }
}


export async function handlePay(message, localPartMailText, userID, bot) {
  let outputMessage;
  const messageWords = message.split(/\s+/);
  let payID = messageWords[messageWords.indexOf("-pay") + 1];
  payID = parseInt(payID);
  let mainOrderArg = messageWords.find(item => item.toLowerCase() === "-m");
  console.log(mainOrderArg, "handlepay");
  const user = getUserNameByID(bot, userID);
  if (messageWords.length === 1) {
    const notPaidUserOrder = await getNotPaidUserOrder(user);
    notPaidUserOrder.map((item, idx )=> {
      const date = moment(item.date).format("DD.MM.YYYY");
      if(idx === 0){
        outputMessage = `Date: ${date}, ID: ${item.id}, Purchaser: ${item.purchaser}, ${item.res} ${item.menu}, Price: ${item.price}€\n`;
      } else {
        outputMessage = outputMessage + `Date: ${date}, ID: ${item.id}, Purchaser: ${item.purchaser}, ${item.res} ${item.menu}, Price: ${item.price}€\n`;
      }
    });
    bot.postMessageToUser(localPartMailText, outputMessage, null);

  } else if (messageWords.length > 1 && mainOrderArg !== "-m") {
    if (payID === "all") {
      markAllUserOrdersWithPaid(user);
    } else if (!isNaN(payID)) {
      markUserOrderWithPaid(user, payID);
    }
  } else if (mainOrderArg === "-m") {
    mainOrderArg = messageWords[messageWords.indexOf("-pay") + 1].toLowerCase();
    payID = messageWords[messageWords.indexOf("-pay") + 2];
    payID = parseInt(payID);
    console.log(messageWords.length, "handlepay");
    if (messageWords.length === 2) {
      const notPaidGroupOrder = await getNotPaidUserOrderAsPurchaser(user);
      console.log(notPaidGroupOrder);
      notPaidGroupOrder.map((item, idx) => {
        const date = moment(item.date).format("DD.MM.YYYY");
        if(idx === 0){
          outputMessage = `Date: ${date}, ID: ${item.id}, User: ${item.user}, ${item.res} ${item.menu}, Price: ${item.price}€\n`;
        } else {
          outputMessage = outputMessage + `Date: ${date}, ID: ${item.id}, User: ${item.user}, ${item.res} ${item.menu}, Price: ${item.price}€\n`;
        }
      });
      bot.postMessageToUser(localPartMailText, outputMessage, null);

    } else if (messageWords.length === 3) {
      if (payID === "all") {
        markAllUserOrdersWithPaidAsPurchaser(user);
      } else if (isNaN(payID) === false) {
        markUserOrderWithPaidAsPurchaser(user, payID);
      } 
    }
  } else {
    outputMessage = "Vergewissern Sie sich, dass Sie dem Befehl die richtigen Argumente mitgegeben haben.";
    bot.postMessageToUser(localPartMailText, outputMessage, null);
    throw new Error("Wrong argument parsed for -pay");
  }
}

export async function handleUnpay(message, localPartMailText, userID, bot) {
  let outputMessage;
  const messageWords = message.split(/\s+/);
  let payID = messageWords[messageWords.indexOf("-unpay") + 1];
  payID = parseInt(payID);
  let mainOrderArg = messageWords.find(item => item.toLowerCase() === "-m");
  const user = getUserNameByID(bot, userID);
  if (messageWords.length === 1) {
    const paidUserOrder = await getPaidUserOrder(user);
    paidUserOrder.map((item, idx )=> {
      const date = moment(item.date).format("DD.MM.YYYY");
      if(idx === 0){
        outputMessage = `Date: ${date}, ID: ${item.id}, Purchaser: ${item.purchaser}, ${item.res} ${item.menu}, Price: ${item.price}€\n`;
      } else {
        outputMessage = outputMessage + `Date: ${date}, ID: ${item.id}, Purchaser: ${item.purchaser}, ${item.res} ${item.menu}, Price: ${item.price}€\n`;
      }
    });
    bot.postMessageToUser(localPartMailText, outputMessage, null);

  } else if (messageWords.length > 1 && mainOrderArg !== "-m") {
    if (payID === "all") {
      markAllUserOrdersWithNotPaid(user);
    } else if (!isNaN(payID)) {
      markUserOrderWithNotPaid(user, payID);
    }
    console.log(mainOrderArg);
  } else if (mainOrderArg === "-m") {
    mainOrderArg = messageWords[messageWords.indexOf("-unpay") + 1].toLowerCase();
    payID = messageWords[messageWords.indexOf("-unpay") + 2];
    payID = parseInt(payID);
    if (messageWords.length === 2) {
      const paidGroupOrder = await getPaidUserOrderAsPurchaser(user);
      paidGroupOrder.map((item, idx) => {
        const date = moment(item.date).format("DD.MM.YYYY");
        if(idx === 0){
          outputMessage = `Date: ${date}, ID: ${item.id}, User: ${item.user}, ${item.res} ${item.menu}, Price: ${item.price}€\n`;
        } else {
          outputMessage = outputMessage + `Date: ${date}, ID: ${item.id}, User: ${item.user}, ${item.res} ${item.menu}, Price: ${item.price}€\n`;
        }
      });
      bot.postMessageToUser(localPartMailText, outputMessage, null);

    } else if (messageWords.length === 3) {
      if (payID === "all") {
        markAllUserOrdersWithNotPaidAsPurchaser(user);
      } else if (isNaN(payID) === false) {
        markUserOrderWithNotPaidAsPurchaser(user, payID);
      } 
    }
  } else {
    outputMessage = "Vergewissern Sie sich, dass Sie dem Befehl die richtigen Argumente mitgegeben haben.";
    bot.postMessageToUser(localPartMailText, outputMessage, null);
    throw new Error("Wrong argument parsed for -pay");
  }
  
}

export async function handleOrderMessage(
  mainMessageID,
  commandUserID,
  channel,
  restaurant,
  bot,
) {
  let order = await fetchOrderMessageData(mainMessageID, channel);
  let currentMessageID = await getCurrentOrders();
  currentMessageID = currentMessageID.find(
    item => item.slack_msg_id === mainMessageID
  );

  if (currentMessageID !== undefined) {
    order = order
      .map(text => {
        let textItem;
        //TODO: better use a regex
        const menu = hitomi.lunchMenu.find(menuItem => {
          textItem = text.message + " ";
          const menuKey = menuItem.key;
          const included = textItem.includes(menuKey);
          return included;
        });
        console.log(mainMessageID, text.user, "WAT");
        //these data are unnecassary for the sms except menuKey, but nice to have for db
        return {
          menuKey: menu ? menu.key.trim() : undefined,
          menu: menu ? menu.name : undefined,
          price: menu ? menu.price : undefined,
          user: getUserNameByID(bot, text.user),
          paid: commandUserID === text.user ? 1 : 0,
          slack_msg_id: text.slack_msg_id,
          slack_thread_id: text.slack_thread_id
        };
      })
      .filter(item => item.menu !== undefined);
    console.log(" ORDER =>",order);

    const sms = order.map(el => {
      insertUserOrder(el.user, getUserNameByID(bot, commandUserID), restaurant, `${el.menuKey.toUpperCase()}: ${el.menu}`, el.price, el.paid, el.slack_msg_id, el.slack_thread_id);
      return el.menuKey;
    });

    deleteCurrentOrderByMsgId(mainMessageID);
    console.log(sms, "SMS");

    if (sms.length !== 0) {
      let newSms = [];
      const map = new Map();
      for (const menuItem of sms) {
        const existing = map.get(menuItem);
        if (existing) {
          map.set(menuItem, existing + 1);
        } else {
          map.set(menuItem, 1);
        }
      }
      const orderUser = getUserNameByID(bot, commandUserID);
      newSms = Array.from(map.entries()).map(
        ([menuItem, count]) => `${count}x ${menuItem.toUpperCase()}`
      );
      insertGroupOrder(orderUser, restaurant, sms.length, mainMessageID);
      newSms = newSms.join("\n");
      await postThread(TOKEN[1], channel, newSms, mainMessageID);

      console.log(newSms, "=> newSMS");
    }
  } else {
    throw new Error("There is no Order with that slack_msg_id");
  } 
}

export async function fetchOrderMessageData(mainMessageID, channel) {
  try {
    const response = await axios.get(
      `https://slack.com/api/conversations.replies?token=${
        TOKEN[1]
      }&channel=${channel}&ts=${mainMessageID}`
    );
    const json = response.data;
    const messages = json.messages.map(data => {
      const message = data.text;
      const user = data.user === undefined ? data.username : data.user;
      const paid = null;
      const slack_msg_id = data.thread_ts;
      const slack_thread_id = data.ts;
      return {
        message,
        user,
        paid,
        slack_msg_id,
        slack_thread_id
      };
    });
    return messages;
  } catch (err) {
    console.error(err);
  }
}