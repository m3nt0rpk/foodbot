CREATE TABLE IF NOT EXISTS group_order
(
  group_order_id INTEGER PRIMARY KEY,
  date           VARCHAR NOT NULL,
  user           VARCHAR NOT NULL,
  restaurant     VARCHAR NOT NULL,
  orders         INTEGER NOT NULL,
  slack_msg_id   VARCHAR
);

CREATE TABLE IF NOT EXISTS user_order
(
  user_order_id   INTEGER PRIMARY KEY,
  date            VARCHAR NOT NULL,
  user            VARCHAR NOT NULL,
  purchaser       VARCHAR NOT NULL,
  restaurant      VARCHAR NOT NULL,
  menu            VARCHAR,
  price           REAL NOT NULL,
  -- Booleans in sqlite are integers (either 0 or 1)
  paid            BOOLEAN NOT NULL,
  slack_msg_id    VARCHAR,
  slack_thread_id VARCHAR
);

CREATE TABLE IF NOT EXISTS current_order
(
  id           integer PRIMARY KEY,
  date         VARCHAR NOT NULL,
  deadline     VARCHAR,
  restaurant   VARCHAR NOT NULL,
  purchaser    VARCHAR NOT NULL,
  slack_msg_id VARCHAR
);