import Slackbot from "slackbots";

import TOKEN from "./tokens";
import {
  handleOrder,
  showInformation,
  handleUnpay,
  handlePay,
  cancelCurrentOrder,
  handleMessageOrder,
  handleCurrentOrder
} from "./handleMessage";
import {
  getlocalPartMailText,
} from "./helper";
import {createTables} from "./db";
import logger from "./logger.mjs";
import info from "./commands";


function handleMessage(
  message,
  messageID,
  channel,
  userID,
  botName,
  localPartMailText
) {
  // gives all commands and how to use it properly
  if (message.includes("-help") && botName !== "FoodBot") {
    bot.postMessageToUser(localPartMailText, info[0], null);
  }
  //order command
  else if (message.includes("-o") && botName !== "FoodBot") {
    handleMessageOrder(message, userID, bot);
  }
  //post running orders to you
  else if (message.includes("-co") && botName !== "FoodBot") {
    handleCurrentOrder(localPartMailText, bot);
  }
  //-cancel aborts an running order if there is one
  else if (message.includes("-cancel") && botName !== "FoodBot") {
    cancelCurrentOrder(message, localPartMailText, userID, bot);
  }
  //post your unpaid menus to the user and marks a unpaid order as paid
  else if (message.includes("-pay") && botName !== "FoodBot") {
    handlePay(message, localPartMailText, userID, bot);  
  } 
  //post your paid menus to the user and marks a paid order as unpaid
  else if (message.includes("-unpay") && botName !== "FoodBot") {
    handleUnpay(message, localPartMailText, userID, bot);  
  } 
  //shows your orders/price by a day count
  else if (message.includes("-s")) {
    showInformation(message, localPartMailText, userID, bot);
  }
  // func will be only called by bot it post the actual order message that was called by a user
  else if (
    botName === "FoodBot" &&
    message.includes("Bestellung") &&
    message.includes("<!channel>")
  ) {
    handleOrder(messageID, message, channel, bot);
  } else {
    logger.info("handleMessage was triggerd but nothing was handled");
    return;
  }
}

const bot = new Slackbot({
  token: TOKEN[0],
  name: "FoodBot"
});

bot.on("start", () => {
  bot.postMessageToChannel("food-supply", "Ich bin jetzt startklar!", null);
  createTables();
});

// handle each message event from an user or bot
bot.on("message", data => {
  if (data.type === "message" && data.message === undefined) {
    logger.debug("Received message event: ", data);
    const messageID = data.ts;
    const message = data.text;

    const channel = data.channel;
    const botName = data.username;
    const userID = data.user;
    let localPartMailText;
    if (botName !== "FoodBot") {
      localPartMailText = getlocalPartMailText(bot, data);
    }

    handleMessage(
      message,
      messageID,
      channel,
      userID,
      botName,
      localPartMailText
    );
  } else {
    return;
  }
});
