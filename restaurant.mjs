const hitomi = {
  lunchMenu: [
    {
      key: "m1 ",
      name: "Tori Don",
      ingredients: "Gegrillte Hühnerbrust auf Reis mit Terriyaki Sauce",
      price: 6.2
    },
    {
      key: "m2 ",
      name: "Fisch Don",
      ingredients: "Gegrillte Fischsorten mit Gemüse auf Reis",
      price: 6.5
    },
    {
      key: "m3 ",
      name: "Agamo Don",
      ingredients: "Knusprige Ente auf Reis mit Terriyaki Sauce",
      price: 7.0
    },
    {
      key: "m4 ",
      name: "Sake Don",
      ingredients: "Gegrillter Lachs auf Reis mit Terriyaki Sauce",
      price: 7.0
    },
    {
      key: "m5 ",
      name: "Bulgogi Don",
      ingredients: "Mariniertes Rindsfleisch nach koreanischer Art",
      price: 7.0
    },
    {
      key: "m6 ",
      name: "Noodle with chicken",
      ingredients: "Gebratene Nudeln mit Hühnerfleisch",
      price: 6.4
    },
    {
      key: "m7 ",
      name: "Agamo Noodle",
      ingredients: "Gebratene Nudeln mit Ente",
      price: 7.0
    },
    {
      key: "m8 ",
      name: "Wok Gemüse",
      ingredients: "gebratenes Gemüse",
      price: 6.5
    },
    {
      key: "m9 ",
      name: "Wok Tofu",
      ingredients: "gebratener Tofu mit Gemüse",
      price: 6.5
    },
    {
      key: "m10",
      name: "Thai Chicken Curry",
      ingredients: "Hühnerfleisch mit Gemüse in Thai Currysauce",
      price: 6.8
    },
    {
      key: "m11",
      name: "Avocado Huhn",
      ingredients: "Gebratenes Hühnerfleisch mit Gemüse in Avocadosauce",
      price: 6.8
    },
    {
      key: "m12",
      name: "Mango Ente",
      ingredients: "Knusprige Ente mit frischen Mango in Thai-Mangosauce",
      price: 7.5
    },
    {
      key: "m13",
      name: "Tori Terriyaki",
      ingredients: "Hühnerfleisch mit Gemüse in Terriyaki Sauce",
      price: 6.8
    },
    {
      key: "m14",
      name: "Teppanyaki Reis",
      ingredients: "Gebratener Reis mit Gemüse",
      price: 6.2
    },
    {
      key: "m15",
      name: "Chicken with Vegetables",
      ingredients: "Hühnerfleisch mit Gemüse",
      price: 6.8
    },
    {
      key: "m16",
      name: "Crispy Duck",
      ingredients: "Knusprige Ente mit Sojasprossen",
      price: 7.5
    },
    {
      key: "m17",
      name: "Rindsfleisch mit Gemüse",
      ingredients: "Rindsfleisch mit Gemüse",
      price: 7.0
    },
    {
      key: "m18",
      name: "Rindsfleisch mit gebratenen Nudeln",
      ingredients: "Rindsfleisch mit gebratenen Nudeln",
      price: 7.2
    },
    {
      key: "m19",
      name: "Knuspriges Huhn mit Sojasprossen",
      ingredients: "Knuspriges Huhn mit Sojasprossen",
      price: 7.0
    }
  ]
};

export default hitomi;