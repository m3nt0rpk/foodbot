const INFO =
  "`-o` erstellt eine neue Essensbestellung, um eine Bestellung aufzugeben ist es nötig nach dem Befehl den Restaurantnamen mitzugeben.\n\n`-t` benötigt eine Uhrzeit als Argument und MUSS derzeit in Kombination mit dem Befehl `-o` benutzt werden. Bsp.: `-o hitomi -t 12:00`\n\n`-co` zeiget Ihnen alle laufenden Bestellungen an.\n\n`-cancel` bricht eine laufende Bestellung ab, dafür muss man die ID der laufenden Bestellung mitgeben. Die ID einer Bestellung kann man mit `-co` herausfinden. Wenn es immer nur eine laufende Bestellung geben wird, dann ist die ID immer '1'. `-cancel` kann nur von dem User benutzt werden, welcher auch die Bestellung aufgegeben hat.\n\n`-s` müssen `price` order `menu` mitgegeben werden und eine Anzahl von Tagen.\n`menu` gibt Ihnen die Menüs zurück, welche Sie in den letzten X Tagen bestellt haben.\n`price` gibt Ihnen die Summe der ausgegebenen Kosten von den Menüs wieder, welche Sie in den letzten X Tagen bestellt haben. Bsp.:`-s price 30`\n\n";
const ORDER = "testorder";

const commands = [INFO, ORDER];

module.exports = commands;
