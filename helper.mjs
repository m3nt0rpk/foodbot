import axios from "axios";

//get botID
export function getBotId(bot) {
  const users = bot.getUsers();
  const userID = users._value.members
    .map(name => (name.name === "foodbot" ? name.id : null))
    .find(name => name !== null);
  return userID;
}
// get bot name
export function getUserNameByID(bot, userID) {
  const users = bot.getUsers();
  const userName = users._value.members
    .map(item => (item.id === userID ? item.real_name : null))
    .find(name => name !== null);
  return userName;
}

export function getlocalPartMailText(bot, dataFromMessage) {
  const users = bot.getUsers();
  const userName = users._value.members
    .map(ID => (ID.id === dataFromMessage.user ? ID.name : null))
    .find(name => name !== null);
  return userName;
}

export async function postThread(token, channel, text, thread_ts) {
  try {
    const message = await axios.post(
      `https://slack.com/api/chat.postMessage?token=${token}&channel=${channel}&text=${text}&thread_ts=${thread_ts}&icon_url=%2FFoodBot.jpg`
    );
    const json = message.data;
    return json;
  } catch (err) {
    console.error(err);
  }
}