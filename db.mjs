import fs from "fs";
import sqlite3 from "sqlite3";
sqlite3.verbose();

import logger from "./logger.mjs";

const db = new sqlite3.Database("./foodDB.db", err => {
  if (err) {
    return console.error(err.message);
  }
  logger.info("Connected to the foodDB database.");
});

function dbRunPromise(sql, params) {
  return new Promise((resolve, reject) => {
    db.run(sql, params, function (err) {
      if (err) {
        reject(err);
      } else {
        resolve({lastID: this.lastID, changes: this.changes});
      }
    });
  });
}

function dbGetPromise(sql, params) {
  return new Promise((resolve, reject) => {
    db.get(sql, params, (err, row) => {
      if (err) {
        reject(err);
      } else {
        resolve(row);
      }
    });
  });
}

function dbAllPromise(sql, params) {
  return new Promise((resolve, reject) => {
    db.all(sql, params, (err, row) => {
      if (err) {
        reject(err);
      } else {
        resolve(row);
      }
    });
  });
}

export function deleteCurrentOrderByMsgId(slack_msg_id){
  return dbRunPromise("DELETE from current_order WHERE slack_msg_id = ?",[slack_msg_id]);
}

export function deleteCurrentOrderById(id){
  return dbRunPromise("DELETE from current_order WHERE id = ?",[id]);
}

export function insertNewCurrentOrder(deadline, purchaser, restaurant, slackMessageId) {
  const deadlineString = deadline.format();
  return dbRunPromise("INSERT INTO current_order (date, deadline, purchaser, slack_msg_id, restaurant) VALUES (datetime('now', 'localtime'), ?, ?, ?, ?)", [
    deadlineString, purchaser, slackMessageId, restaurant
  ]);
}

export function insertUserOrder(user, purchaser, restaurant, menu, price, paid, msg_id, thread_id){
  return dbRunPromise("INSERT INTO user_order (date, user, purchaser, restaurant, menu, price, paid, slack_msg_id, slack_thread_id) VALUES(datetime('now', 'localtime'),?,?,?,?,?,?,?,?)",[user, purchaser, restaurant, menu, price, paid, msg_id, thread_id]);
}

export function insertGroupOrder(user, restaurant, orderCount, msg_id){
  return dbRunPromise("INSERT INTO group_order (date, user, restaurant, orders, slack_msg_id) VALUES(datetime('now', 'localtime'),?,?,?,?)",[user, restaurant, orderCount, msg_id]);
}

export function getCurrentOrders(){
  return dbAllPromise("SELECT * FROM current_order", []);
}

export function getLatestCurrentOrder(restaurant) {
  return dbGetPromise("SELECT * FROM current_order WHERE restaurant = ? ORDER BY date DESC LIMIT 1", [restaurant]);
}

export function getOrdersPriceSum(user, days){
  return dbGetPromise("SELECT sum(price) as sum_price FROM user_order WHERE user = ? AND date > (SELECT datetime('now', ?))", [user, `-${days} day`]);
}

export function getOrderMenus(user, days){
  return dbAllPromise("SELECT restaurant res, menu, price FROM user_order WHERE user = ? AND date > (SELECT datetime('now', ?))", [user, `-${days} day`]);
}

export function getNotPaidUserOrder(user){
  return dbAllPromise("SELECT user_order_id id, purchaser, restaurant res, menu, price, date FROM user_order WHERE paid = ? AND user = ?",[0, user]);
}

export function getNotPaidUserOrderAsPurchaser(user){
  return dbAllPromise("SELECT user_order_id id, user, restaurant res, menu, price, date FROM user_order WHERE paid = ? AND purchaser = ?", [0, user]);
}

export function updateCurrentOrderSlackMsgId(messageID, restaurant){
  return dbGetPromise("UPDATE current_order SET slack_msg_id = ? WHERE restaurant = ?", [messageID ,restaurant]);
}

export function markAllUserOrdersWithPaid(user){
  return dbRunPromise("UPDATE user_order SET paid = ? WHERE paid = ? AND user = ?",[1, 0, user]);
}

export function markUserOrderWithPaid(user, payID){
  return dbRunPromise("UPDATE user_order SET paid = ? WHERE paid = ? AND user = ? AND user_order_id = ?",[1, 0, user, payID]);
}

export function markAllUserOrdersWithPaidAsPurchaser(purchaser){
  return dbRunPromise("UPDATE user_order SET paid = ? WHERE paid = ? AND purchaser = ?",[1, 0, purchaser]);
}

export function markUserOrderWithPaidAsPurchaser(purchaser, payID){
  return dbRunPromise("UPDATE user_order SET paid = ? WHERE paid = ? AND purchaser = ? AND user_order_id = ?",[1, 0, purchaser, payID]);
}

export function getPaidUserOrder(user){
  return dbAllPromise("SELECT user_order_id id, purchaser, restaurant res, menu, price, date FROM user_order WHERE paid = ? AND user = ?",[1, user]);
}

export function markAllUserOrdersWithNotPaid(user){
  return dbRunPromise("UPDATE user_order SET paid = ? WHERE paid = ? AND user = ?",[0, 1, user]);
}

export function markUserOrderWithNotPaid(user, payID){
  return dbRunPromise("UPDATE user_order SET paid = ? WHERE paid = ? AND user = ? AND user_order_id = ?",[0, 1, user, payID]);
}

export function getPaidUserOrderAsPurchaser(user){
  return dbAllPromise("SELECT user_order_id id, user, restaurant res, menu, price, date FROM user_order WHERE paid = ? AND purchaser = ?", [1, user]);
}

export function markAllUserOrdersWithNotPaidAsPurchaser(purchaser){
  return dbRunPromise("UPDATE user_order SET paid = ? WHERE paid = ? AND purchaser = ?",[0, 1, purchaser]);
}

export function markUserOrderWithNotPaidAsPurchaser(purchaser, payID){
  return dbRunPromise("UPDATE user_order SET paid = ? WHERE paid = ? AND purchaser = ? AND user_order_id = ?",[0, 1, purchaser, payID]);
}

export function createTables() {
  const buf = fs.readFileSync("create-tables.sql");
  const sqlString = buf.toString();
  const promises = [];
  const statements = sqlString.split(";").filter(s => s.length > 0);
  for (const statement of statements) {
    const st = statement.trim();
    logger.debug("Running statement", {statement: st});
    promises.push(dbRunPromise(st));
  }
  return Promise.all(promises);
}

export default db;
